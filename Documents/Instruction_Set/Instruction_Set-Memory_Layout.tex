\section*{Memory and Register Layout}

This section contains a short overview of the \dwmc Memory and Register Layout.

\subsection*{Memory Layout}

The \dwmc has a 16 bit wide data bus with a 24 bit address bus, the system can access up to 16MiWords (32MiByte) is Random Access Memory. It is organised into 256 segments of 64 kiWords (128 kiByte) for ease of addressing, as well as keeping code slightly shorter.

The first segment from 0x000000 to 0x00FFFF is reserved as 'fast' system and OS memory. Of this the first 16 Words are reserved for the Reset and Interrupt Vectors, which can easily be changed by the OS or external Code. The Stack also resides within this Segment, counting down from 0x00FFFF, with a maximum size if 16kiWords.

The second segment from 0x010000 to 0x01FFFF contains the address space memory mapped IO Hardware.

This is followed by six segments from 0x020000 to 0x07FFFF, containing the systems BIOS and the main OS.

The BIOS is contained within the third Memory Segment from 0x020000 to 0x02FFFF. It is made up of the Boot Loader, a simple Monitor OS and simple IO subroutines that can be used by the Main OS and every other program.

The rest of the systems memory segments is general use RAM.

\begin{figure}[h]
	\centering
    \ttfamily\scriptsize
    \begin{tabular}{cc}
    	\adjustbox{valign=m}{
    		\subfloat[Main Memory]{
    		\begin{bytefield}{26}
    			\begin{rightwordgroup}{RAM}
    				\memsection{000000}{00000f}{2}{Interrupt Vectors}\\
    				\memsection{000010}{00001f}{2}{Special Registers}\\
    				\memsection{000010}{00ffff}{4}{System/OS Memory}
	    		\end{rightwordgroup}\\
    			\memsection{010000}{01ffff}{4}{Memory Mapped IO}\\
    			\begin{rightwordgroup}{Flash\\Memory}
    				\memsection{020000}{02ffff}{2}{BIOS}\\
    				\memsection{030000}{07ffff}{6}{Main OS}
	    		\end{rightwordgroup}\\
    			\begin{rightwordgroup}{RAM}
    				\memsection{080000}{ffffff}{10}{General Use RAM}
    			\end{rightwordgroup}\\
	    	\end{bytefield}
	    }}
    	&
    	\adjustbox{valign=m}{
    		\begin{tabular}{@{}c@{}}
	    		\subfloat[Interrupt Vectors]{
		    		\begin{bytefield}{16}
    					\memsection{000000}{}{2}{Reset Vector}\\
	    				\memsection{000002}{}{2}{Interrupt Vector 1}\\
    					\memsection{000004}{}{2}{Interrupt Vector 2}\\
	    				\memsection{000006}{}{2}{Interrupt Vector 3}\\
	    				\memsection{000008}{}{2}{Interrupt Vector 4}\\
    					\memsection{00000A}{}{2}{Interrupt Vector 5}\\
	    				\memsection{00000C}{}{2}{Interrupt Vector 6}\\
		    			\memsection{00000E}{}{2}{Interrupt Vector 7}\\
	    			\end{bytefield}
	    		}\\
    			\subfloat[Special Registers]{
			    	\begin{bytefield}{6}
    					\memsection{000010}{}{2}{Program Counter}\\
    					\memsection{000012}{}{2}{Stack Pointer}\\
    					\memsection{000014}{}{2}{Task Pointer}\\
    					\memsection{000016}{}{2}{Interrupt Pointer}\\
    					\memsection{00001A}{}{2}{Y Index Pointer}\\
    					\memsection{00001C}{}{2}{Z Index Pointer}\\
    					\memsection{00001E}{}{2}{Y Offset Register}\\
    					\memsection{00001F}{}{2}{Z Offset Register}
    				\end{bytefield}
    			}
    		\end{tabular}
    	}
	\end{tabular}
    \caption{Memory Layout}
    \normalfont\normalsize
\end{figure}

\subsection*{Register Bank}

The \dwmc has a register bank with 16 registers of which several are meant to be special use registers.

Any registers within the register bank can be addressed and accessed like any other register. This allows to \texttt{push} and \texttt{pop} all registers (safe the Stack Pointer) to/from the stack.

Additionally, some of the flags within the Flag Register (R15) can only be set by the hardware and can not be overwritten by overwriting the Flag Register.\\

\vspace{1em}
\centerline{
   \ttfamily\footnotesize
    \begin{tabular}{|c|c|c|c|c|c|c|c|}
        \hline 
        R00 & R01 & R02 & R03 & R04 & R05 & R06 & R07 \\ 
        \hline 
        R08 & R09 & R10 & R11 & R12 & R13 & R14 & R15/F \\ 
        \hline 
    \end{tabular}  
    \normalfont\normalsize
}
\captionof{table}{Register Bank}

\subsection*{Special Registers}

Special use registers, such as the Program Counter, Stack Pointers and Index Pointers are, as far as logical access to them is concerned, located in the first segment of the main memory, after the Interrupt Vectors. They can easily be accessed like any other part of the main memory with load and store operations, as well as direct access with other operations.

\subsection*{Flag Register}

The Flag register is a special use register that is used by the Control Logic to control the program flow and contains all flags of the system. It can be operated on in normal operation like any other register, e.g. pushed and poped to/from the Stack, for subroutine operations.\\

\begin{figure}[h]
    \centering
    \ttfamily\footnotesize
    \begin{bytefield}[endianness=little, bitwidth=1.75em]{16}
        \bitheader{0-15} \\
        \bitbox{1}{Z} & \bitbox{1}{0} & \bitbox{1}{C} & \bitbox{1}{QC} &
        \bitbox{1}{HC} & \bitbox{1}{TC} & \bitbox{1}[bgcolor=lightgray]{} & \bitbox{1}{EQ} &
        \bitbox{1}{N} & \bitbox{1}{OF} & \bitbox{1}[bgcolor=lightgray]{} & \bitbox{1}[bgcolor=lightgray]{} &
        \bitbox{1}{IE} & \bitbox{1}{I2} & \bitbox{1}{I1} & \bitbox{1}{I0}
    \end{bytefield}
    \caption{Flag Register Layout}
    \normalfont\normalsize
\end{figure}

\begin{table}[h!]
    \centering
    \ttfamily\small
    \begin{tabular}{|c|c|c|c|}
        \hline 
        Z & Zero Flag & N & Negative Flag \\ 
        \hline 
        C & Carry Flag & OF & Overflow Flag \\ 
        \hline 
        QC & Quarter Carry Flag & IE & Interrupt Enable Flag \\ 
        \hline 
        HC & Half Carry Flag & I2 & Interrupt Flag 2 \\ 
        \hline 
        TC & Three Quarter Carry Flag & I1 & Interrupt Flag 1 \\ 
        \hline 
        EQ & Equal Flag & I0 & Interrupt Flag 0 \\ 
        \hline 
    \end{tabular} 
    \normalfont\normalsize
    \caption{Flags}
\end{table}

Of these flags, the \texttt{IO}, \texttt{IE} and \texttt{I0-I2} Flag may be of special interest.\\
Internally to the \dwmc CPU the \texttt{IO} is unimportant and is only of use to access external IO devices, such as serial communication ports or mass storage devices.\\
The \texttt{IE} flag is important for the CPUs handling of interrupts. If the \texttt{IE} flag is enabled, the \dwmc can react to Interrupts, but during interrupt handling routines, the \texttt{IE} flag has to be disabled.\\
Finally the \texttt{I0-I2} flags, cannot be set internally by the \dwmc CPU or any code itself. Instead, these flags are connected to a four layer FIFO memory, allowing the \dwmc to react to interrupts coming over a short time frame in consecutive order. It also allows for Interrupt handling should a interrupt occur when another Interrupt is being handled.