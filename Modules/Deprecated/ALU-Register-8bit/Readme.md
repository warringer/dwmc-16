# ALU Register Module 8 bit with Write Enable

This Module is a small 8 bit Register Module meant to be used in the
DWMC-16 ALU. It has independent data input and output.

Outputs are avaiable at any time just to the ALU, while the writing is
done via a Write Enable on the raising edge of a Trigger signal.

It is 8 bit wide to ensure that only a minimum number of different
components is used, in this case a 32 pin DIN41612 connector.
