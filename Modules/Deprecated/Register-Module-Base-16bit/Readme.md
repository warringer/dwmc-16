# Basic Register Module 16 bit with Read and Write Enable

This module is an Eurocard sized PCB containing the discrete electronics
for a basic 16 bit wide register with a tristate output (Read enable)
and a positively fnak triggered write enabled input.
