![DWMC-16](DWMC-16.png)

This is the main repository for my DWMC-16 DIWhy minicomputer.

The project is something of a brainfart that would not leave me alone
after watching too many videos on [Usage Electric](https://www.youtube.com/@UsagiElectric) on Youtube.

It just made me want to build my own 16 bit MiniComputer from scratch.

And I mean it when I say from scratch. From the logic design, over the
Instruction Set to the schematics for the almost 100% discrete logic
modules and their PCBs. (Its not 100% because like heck am I going to
design things like memory or storage from the ground up...)

Mainly this repository is meant to contain the main files I am using
for the design and construction of the DWMC-16, while I document my
work over on my blog [Warringers Worlds](http://warringersworlds.net).

Hardware schematics and PCBs are created with [KiCAD](https://www.kicad.org).

Any hardware developed for this project is open source under the
CERN Open Hardware Licence Version 2 - Permissive CERN-OHL-P-2.0+.

Any software developed for this pronect is open source under the
European Union Public License, version 1.2 EUPL-1.2+.
